import { AcceptSuggestionOperation } from './operations/AcceptSuggestionOperation';
import { QueueMaker } from './infrastructure/QueueMaker';
import { AssignChallengeOperation } from './operations/AssignChallengeOperation';
import { CompleteChallengeOperation } from './operations/CompleteChallengeOperation';
import { SuggestChallengeOperation } from './operations/SuggestChallengeOperation'
import * as firebase from 'firebase-admin';
import * as http from 'http';
import * as sharp from 'sharp';
import * as fs from 'fs';

console.log('Running in ' + process.env.NODE_ENV + ' mode');

var app = firebase.initializeApp({
  credential: firebase.credential.cert('config/art-app-22a6c-firebase-adminsdk-xxzfj-81995eb3e0.json'),
  databaseURL: "https://art-app-22a6c.firebaseio.com",
  databaseAuthVariableOverride: {
    uid: 'service-bot',
    canProcessTasks: true
  }
});




// var pipeline = sharp()
//   .png()
//   .rotate()
//   .resize(500)
//   .blur(2)
//   .greyscale()
//   .on('info', info => {
//     console.log('Info! ', info);
//   });

// var buffers = [];
// var buffer;
// file.createReadStream()
//   .on('error', (error) => {
//     console.error(error);
//   })
//   .on('response', (response) => {
//     console.log('Response received!');
//   })
//   .on('data', (data) => {
//     console.log('Data chunk received: ', data);
//     buffers.push(data);
//   })
//   .on('end', () => {
//     console.log('It has ended');
//   }).pipe(pipeline).pipe(fs.createWriteStream('test2.png'));


var queueRef = (<any>firebase.database()).ref('queue');
QueueMaker.setQueueReference(queueRef);

var queueCollection = [];

queueCollection[0] = QueueMaker.createQueue(new SuggestChallengeOperation(), 'suggest challenge');
queueCollection[1] = QueueMaker.createQueue(new AcceptSuggestionOperation(), 'accept suggestions');
queueCollection[2] = QueueMaker.createQueue(new AssignChallengeOperation(), 'assign challenge');
queueCollection[3] = QueueMaker.createQueue(new CompleteChallengeOperation(), 'complete challenge');

process.on('SIGINT', stopQueues);
process.on('SIGTERM', stopQueues);

function stopQueues() {
  console.log('Starting queue shutdown');
  var shutdownPromises = [];
  queueCollection.forEach(queue => {
    var shutdownPromise = queue.shutdown().then(() => {
      console.log('Finished queue shutdown');
    });
    shutdownPromises.push(shutdownPromise);
  });

  return Promise.all(shutdownPromises).then((done) => {
    console.log('All queues have shut down. Exiting...');
    process.exit(0);
    return 0;
  });
}

http.createServer((request, response) => {
  response.writeHead(403, {
    'Content-Type': 'text/plain',
    'Access-Controll-Allow-Origin': '*'
  });
}).listen(process.env.PORT || 5000);

