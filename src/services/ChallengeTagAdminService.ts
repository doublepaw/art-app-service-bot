import { ChallengeTag, Challenge } from 'art-app-common';
import * as firebase from 'firebase-admin';
import { KeyValue, ISnapShot } from 'art-app-common';

export class ChallengeTagAdminService {
  private static challengeTagsRefString: string = 'challengeTags';
  private challengeTagsRef: any;

  constructor() {
    var databaseRef = (firebase.database() as any).ref();
    this.challengeTagsRef = databaseRef.child(ChallengeTagAdminService.challengeTagsRefString);
  }

  public get = () => {
    return new Promise<KeyValue<ChallengeTag>[]>((resolve, reject) => {
      this.challengeTagsRef.once('value', (challengeTagSnapShots: ISnapShot<ChallengeTag>[]) => {
        var challengeTags: KeyValue<ChallengeTag>[] = [];

        challengeTagSnapShots.forEach(snapShot => {
          var challengeTag = this.mapChallengeTagKeyValue(snapShot);
          challengeTags.push(challengeTag);
          return false;
        });

        return resolve(challengeTags);
      }, reason => {
        return reject(reason);
      });
    });
  };

  private mapChallengeTagKeyValue = (snapShot: ISnapShot<ChallengeTag>) => {
    var challengeTag = new ChallengeTag(snapShot.val().name, snapShot.val().type);
    return new KeyValue(snapShot.key, challengeTag);
  };

  public delete = (challengeTagId: string) => {
    return new Promise<void>((resolve, reject) => {
      if (challengeTagId && challengeTagId.length > 0) {
        var challengeTagRef = this.challengeTagsRef.child(challengeTagId);
        return challengeTagRef.set(null).then(() => resolve(), () => reject());
      }

      reject();
    });
  };

  public update = (challengeTagId: string, challengeTag: ChallengeTag) => {
    return new Promise<KeyValue<ChallengeTag>>((resolve, reject) => {
      if (challengeTagId && challengeTagId.length > 0) {
        var tagRef = this.challengeTagsRef.child(challengeTagId);

        return tagRef.set(challengeTag).then(() => {
          return resolve(new KeyValue(challengeTagId, challengeTag));
        }, () => reject());
      }

      reject();
    });
  };

  public add = (challengeTag: ChallengeTag) => {
    return new Promise<KeyValue<ChallengeTag>>((resolve, reject) => {
      var newChallengeTagRef = this.challengeTagsRef.push();
      newChallengeTagRef.set(challengeTag).then(() => {
        return resolve(new KeyValue<ChallengeTag>(newChallengeTagRef.key, challengeTag));
      }, (error) => {
        return reject(error);
      });
    });
  }
}