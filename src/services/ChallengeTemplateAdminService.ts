import {
  Challenge,
  ChallengeTemplate,
  ChallengeTemplateOwner,
  ChallengeTemplateOwnerType,
  ChallengeTemplateType,
  KeyValue,
  ISnapShot
} from 'art-app-common';
import * as firebase from 'firebase-admin';



export class ChallengeTemplateAdminService {
  private static challengeTemplatesRefString: string = "challengeTemplates";
  private challengeTemplatesRef: any;

  constructor() {
    var databaseRef = (firebase.database() as any).ref();
    this.challengeTemplatesRef = databaseRef.child(ChallengeTemplateAdminService.challengeTemplatesRefString);
  }

  public add = (challengeTemplate: ChallengeTemplate) => {
    if (challengeTemplate) {
      var newRef = this.challengeTemplatesRef.push();

      return new Promise<KeyValue<ChallengeTemplate>>((resolve, reject) => {
        newRef.set(challengeTemplate).then(() => {
          return resolve(new KeyValue(newRef.key, challengeTemplate));
        }).catch((error) => {
          return reject(error);
        });
      });
    }
  }


  public update = (challengeTemplateId: string, template: ChallengeTemplate) => {
    return new Promise<void>((resolve, reject) => {
      if (challengeTemplateId && challengeTemplateId.length > 0) {
        var templateRef = this.challengeTemplatesRef.child(challengeTemplateId);
        return templateRef.set(template).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        });
      }
    });
  };


  public delete = (challengeTemplateId: string) => {
    return new Promise<any>((resolve, reject) => {
      if (challengeTemplateId && challengeTemplateId.length > 0) {
        var templateRef = this.challengeTemplatesRef.child(challengeTemplateId);
        return templateRef.set(null).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        });
      }
    });
  }

  public get = (challengeTemplateId: string) => {
    return new Promise<KeyValue<ChallengeTemplate>>((resolve, reject) => {
      this.challengeTemplatesRef.child(challengeTemplateId).once('value', snapShot => {
        var challengeTemplateKeyValue = this.mapSnapshotToChallengeTemplateKeyValue(snapShot);
        return resolve(challengeTemplateKeyValue);
      }, (error) => {
        return reject(error);
      });
    });
  };

  private mapSnapshotToChallengeTemplateKeyValue = (snapShot: ISnapShot<ChallengeTemplate>) => {
    var challengeTemplate = new ChallengeTemplate();

    challengeTemplate.title = snapShot.val().title || null;
    challengeTemplate.description = snapShot.val().description || null;
    challengeTemplate.type = snapShot.val().type || null;
    challengeTemplate.challengeTagIds = snapShot.val().challengeTagIds || null;
    challengeTemplate.challengeOwner = new ChallengeTemplateOwner(ChallengeTemplateOwnerType.Admin, null);

    return new KeyValue(snapShot.key, challengeTemplate);
  };

  public getAll = () => {
    return new Promise<KeyValue<ChallengeTemplate>[]>((resolve, reject) => {
      this.challengeTemplatesRef.once('value', (snapShots: ISnapShot<ChallengeTemplate>[]) => {
        var challengeTemplateKeyValues: KeyValue<ChallengeTemplate>[] = [];

        snapShots.forEach(snapShot => {
          var challengeTemplate = new ChallengeTemplate();
          challengeTemplate.title = snapShot.val().title || null;
          challengeTemplate.description = snapShot.val().description || null;
          challengeTemplate.type = snapShot.val().type || null;
          challengeTemplate.challengeTagIds = snapShot.val().challengeTagIds || [];
          challengeTemplate.challengeOwner = new ChallengeTemplateOwner(ChallengeTemplateOwnerType.Admin, null);

          challengeTemplateKeyValues.push(new KeyValue(snapShot.key, challengeTemplate));

          return false;
        });

        resolve(challengeTemplateKeyValues);
      }, (reason) => {
        reject(reason);
      });
    });
  }
}