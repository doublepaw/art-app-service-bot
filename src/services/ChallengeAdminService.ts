import { Thenable } from 'bluebird';
import { ChallengeTemplateAdminService } from './ChallengeTemplateAdminService';
import { KeyValue, ISnapShot, Challenge, ChallengeState, ChallengeTemplate } from 'art-app-common';
import { StorageService } from './StorageService';
import * as firebase from 'firebase-admin';

export class ChallengeAdminService {
  private activeChallengesRef: any;
  private completedChallengesRef: any;
  private challengeTemplateService: ChallengeTemplateAdminService;

  constructor() {
    var databaseRef = (firebase.database() as any).ref();
    this.activeChallengesRef = databaseRef.child('activeChallenges');
    this.completedChallengesRef = databaseRef.child('completedChallenges');
    this.challengeTemplateService = new ChallengeTemplateAdminService();
  }

  public assignChallenge = (userId: string, challengeTemplateId: string) => {
    return new Promise<KeyValue<Challenge>>((resolve, reject) => {
      return this.challengeTemplateService.get(challengeTemplateId).then(challengeTemplate => {
        return this.createActiveChallenge(new AssignChallengeData(userId, challengeTemplate));
      }).then(newChallenge => resolve(newChallenge))
        .catch(error => reject(error));
    });
  };

  private createActiveChallenge = (challengeData: AssignChallengeData) => {
    return new Promise<KeyValue<Challenge>>((resolve, reject) => {
      var newChallenge = this.createChallengeFromTemplate(challengeData.challengeTemplate);

      var newChallengeRef = this.activeChallengesRef.child(challengeData.userId).push();
      return newChallengeRef.set(newChallenge).then(() => {
        resolve(new KeyValue(newChallengeRef.key, newChallenge));
      }).catch(error => {
        reject(error);
      });
    });
  };

  public completeChallenge = (userId: string, activeChallengeId: string) => {
    return this.getActiveChallenge(userId, activeChallengeId)
      .then(this.setCompletedChallenge)
      .then(this.removeActiveChallenge)
      .then(() => console.log('WOW! :D'))
      .catch(error => console.log(':C'));
  };

  private getActiveChallenge = (userId: string, activeChallengeId: string) => {
    return new Promise<CompleteChallengeData>((resolve, reject) => {
      var activeChallengeDbRef = this.activeChallengesRef.child(userId).child(activeChallengeId);

      activeChallengeDbRef.once('value').then((snapShot: ISnapShot<Challenge>) => {
        var challenge = this.mapSnapShotToChallengeKeyValue(snapShot);
        resolve(new CompleteChallengeData(userId, challenge));
      }).catch(error => {
        reject(error);
      });
    });
  };

  private setCompletedChallenge = (data: CompleteChallengeData) => {
    return new Promise<CompleteChallengeData>((resolve, reject) => {
      var newCompletedChallengeDbRef = this.completedChallengesRef.child(data.userId).child(data.challenge.key);

      console.log('Getting public url!');

      data.challenge.value.completedDate = new Date(Date.now());
      data.challenge.value.imageUrl = StorageService.createPublicCompletedChallengeUrl(data.userId, data.challenge.key);

      StorageService.createThumbnail(data.userId, data.challenge.key, 128).then(() => {
        newCompletedChallengeDbRef.set(data.challenge.value).then(() => {
          resolve(data);
        }).catch(error => {
          console.error(error);
          reject(error);
        });
      });
    });
  };

  private removeActiveChallenge = (data: CompleteChallengeData) => {
    return new Promise<void>((resolve, reject) => {
      var challengeToRemoveRef = this.activeChallengesRef.child(data.userId).child(data.challenge.key).remove().then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    });
  };

  private mapSnapShotToChallengeKeyValue = (snapShot: ISnapShot<Challenge>) => {
    var challenge = new Challenge();

    challenge.title = snapShot.val().title || null;
    challenge.description = snapShot.val().description || null;
    challenge.state = snapShot.val().state || null;
    challenge.challengeTemplateId = snapShot.val().challengeTemplateId || null;
    challenge.challengeTagIds = snapShot.val().challengeTagIds || null;
    challenge.imageUrl = snapShot.val().imageUrl || null;
    challenge.completedDate = snapShot.val().completedDate || null;

    return new KeyValue(snapShot.key, challenge);
  };

  private createChallengeFromTemplate = (challengeTemplate: KeyValue<ChallengeTemplate>) => {
    var challenge = new Challenge();

    challenge.title = challengeTemplate.value.title;
    challenge.description = challengeTemplate.value.description;
    challenge.state = ChallengeState.Active;
    challenge.challengeTemplateId = challengeTemplate.key;
    challenge.challengeTagIds = challengeTemplate.value.challengeTagIds;
    challenge.imageUrl = null;
    challenge.completedDate = null;

    return challenge;
  };
}

class CompleteChallengeData {
  constructor(userId: string, challenge: KeyValue<Challenge>) {
    this.userId = userId;
    this.challenge = challenge;
  }

  userId: string;
  challenge: KeyValue<Challenge>;
}

class AssignChallengeData {
  constructor(userId: string, challengeTemplate: KeyValue<ChallengeTemplate>) {

    this.userId = userId;
    this.challengeTemplate = challengeTemplate;
  }

  userId: string;
  challengeTemplate: KeyValue<ChallengeTemplate>;
}