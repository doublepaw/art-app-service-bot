import { TaskData } from '../infrastructure/TaskData';
import * as firebase from 'firebase-admin';

export class TaskService {
  private taskRef: any;
  constructor() {
    this.taskRef = (firebase.database() as any).ref('queue/tasks');
  }

  public pushTask = (data: TaskData) => {
    return new Promise<void>((resolve, reject) => {
      var newTaskRef = this.taskRef.push();
      newTaskRef.set(data).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    });
  };
}