import { ChallengeTemplateAdminService } from './ChallengeTemplateAdminService';
import * as firebase from 'firebase-admin';
import { ChallengeTemplate, ChallengeSuggestion, KeyValue, ISnapShot } from 'art-app-common';
import * as _ from 'lodash';

export class ChallengeSuggestionService {
  private static suggestedChallengesRef: string = 'suggestedChallenges';
  private static challengeTemplatesRef: string = 'challengeTemplates';
  private suggestedChallengesRef: any;
  private challengeTemplatesRef: any;
  private challengeTemplateService: ChallengeTemplateAdminService;

  constructor() {
    var databaseRef = (firebase.database() as any).ref();
    this.suggestedChallengesRef = databaseRef.child(ChallengeSuggestionService.suggestedChallengesRef);
    this.challengeTemplateService = new ChallengeTemplateAdminService();
  }

  public makeSuggestion = (userId: string) => {
    return new Promise<KeyValue<ChallengeSuggestion>>((resolve, reject) => {
      this.generateSuggestion(userId).then(suggestion => {
        var newSuggestionRef = this.suggestedChallengesRef.child(userId).push();
        newSuggestionRef.set(suggestion).then(() => {
          resolve(new KeyValue(newSuggestionRef.key, suggestion));
        }).catch(error => {
          reject(error);
        });
      });
    });
  };

  public getSuggestion = (userId: string, challengeSuggestionId: string) => {
    return new Promise<KeyValue<ChallengeSuggestion>>((resolve, reject) => {
      var suggestedChallengeRef = this.suggestedChallengesRef.child(userId).child(challengeSuggestionId);
      suggestedChallengeRef.once('value', (snapShot: ISnapShot<ChallengeSuggestion>) => {
        resolve(new KeyValue(snapShot.key, snapShot.val()));
      }, (error) => {
        reject(error);
      });
    });
  };

  public removeSuggestion = (userId: string, suggestedChallengeId: string) => {
    return new Promise<void>((resolve, reject) => {
      var suggestedChallengeRef = this.suggestedChallengesRef.child(userId).child(suggestedChallengeId);
      suggestedChallengeRef.set(null).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    });
  };

  public suggestionExists = (userId: string, challengeSuggestionId: string) => {
    return new Promise<void>((resolve, reject) => {
      this.suggestedChallengesRef.child(userId).once('value', (snapShots: ISnapShot<ChallengeSuggestion>[]) => {
        console.log('snapShots: ', snapShots);
        var foundIt = false;
        snapShots.forEach(snapShot => {
          console.log('Trying to find suggestion: ', challengeSuggestionId);
          console.log('Is this it?: ', snapShot, snapShot.key);
          if (snapShot.key === challengeSuggestionId) {
            foundIt = true;
          }
        });
        console.log(foundIt ? 'Found it!' : 'Did not find it :C');
        if (!foundIt)
          reject('Suggestion does not exist on this user.');
        else
          resolve();
      });
    });
  }

  private mapChallengeTemplateKeyValue = (snapShot: ISnapShot<ChallengeTemplate>) => {
    var challengeTemplate = new ChallengeTemplate();

    challengeTemplate.title = snapShot.val().title || null;
    challengeTemplate.description = snapShot.val().description || null;
    challengeTemplate.type = snapShot.val().type || null;
    challengeTemplate.challengeTagIds = snapShot.val().challengeTagIds || null;
    challengeTemplate.challengeOwner = snapShot.val().challengeOwner || null;

    return new KeyValue(snapShot.key, challengeTemplate);
  };

  private generateSuggestion = (userId: string) => {
    return new Promise<ChallengeSuggestion>((resolve, reject) => {
      this.challengeTemplateService.getAll().then((challengeTemplates) => {
        var numberOfTemplates = challengeTemplates.length;
        var randomIndex = Math.floor(Math.random() * numberOfTemplates);
        var randomTemplate = challengeTemplates[randomIndex];

        var suggestion = this.mapToSuggestion(randomTemplate);

        return resolve(suggestion);
      }).catch(error => {
        reject(error);
      });
    });
  };

  private mapToSuggestion = (challengeTemplate: KeyValue<ChallengeTemplate>) => {
    var suggestion = new ChallengeSuggestion();

    suggestion.challengeTemplateId = challengeTemplate.key;
    suggestion.description = challengeTemplate.value.description;
    suggestion.challengeTagIds = challengeTemplate.value.challengeTagIds;
    suggestion.title = challengeTemplate.value.title;

    return suggestion;
  };
}