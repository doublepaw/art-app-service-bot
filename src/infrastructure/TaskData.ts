export class TaskData {
    _id?: string;
    _state: string;
    _state_changed?: Date;
    _owner?: string;
    _progress?: number;
    _error_details?: {
        previous_state: string;
        error: string;
        attempts: number;
        error_stack: string;
    };
}