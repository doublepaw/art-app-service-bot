import { Operation } from './Operation';
import { TaskData } from './TaskData';
import * as Queue from 'firebase-queue';

export class QueueMaker {
    private static queueReference: any;

    public static setQueueReference(queueReference: any) {
        this.queueReference = queueReference;
    }

    public static createQueue = <T extends TaskData>(operation: Operation<T>, name: string) => {
        console.log('Creating ' + name + ' queue!');
        return new Queue(QueueMaker.queueReference, operation.getOptions(), operation.jobFunction);
    }
}