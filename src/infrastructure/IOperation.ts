import { TaskData } from './TaskData';
import { QueueOptions } from './QueueOptions';

export interface IOperation<T extends TaskData> {
    jobFunction: (data: T, progress, resolve: (result?: any) => any, reject: (reason?: any) => any) => void;
    getOptions(): QueueOptions;
}