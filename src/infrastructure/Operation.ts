import { IOperation } from './IOperation';
import { TaskData } from './TaskData';
import { QueueOptions } from './QueueOptions';

export abstract class Operation<T extends TaskData> implements IOperation<T> {
    public abstract getOptions(): QueueOptions;
    public abstract jobFunction: (data: T, progress, resolve, reject) => void;
}   