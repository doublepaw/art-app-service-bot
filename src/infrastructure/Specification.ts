export interface Specification {
    start_state: string;
    in_progress_state: string;
    finished_state: string;
    error_state: string;
    timeout: number;
    retries: number;

    // constructor() {
    //     this.start_state = null;
    //     this.in_progress_state = 'in_progress';
    //     this.finished_state = null;
    //     this.error_state = 'error';
    //     this.timeout = 300000;
    //     this.retries = 0;
    // }
}