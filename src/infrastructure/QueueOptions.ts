export class QueueOptions {
    public specId: string;
    public numWorkers: number;
    public sanitize: boolean;
    public suppressStack: boolean;
}
