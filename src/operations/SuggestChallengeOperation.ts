import { Operation } from '../infrastructure/Operation';
import { QueueOptions } from '../infrastructure/QueueOptions';
import { TaskData } from '../infrastructure/TaskData';
import { ChallengeAdminService } from '../services/ChallengeAdminService';
import {ChallengeSuggestionService} from '../services/ChallengeSuggestionService';
import {ChallengeTemplate, ChallengeSuggestion} from 'art-app-common';

export class SuggestChallengeOperation extends Operation<SuggestChallengeOperationTaskData> {
    private challengeService: ChallengeAdminService;
    private challengeSuggestionService: ChallengeSuggestionService;

    constructor() {
        super();
        this.challengeService = new ChallengeAdminService();
        this.challengeSuggestionService = new ChallengeSuggestionService();
    }

    public getOptions() {
        return { specId: 'suggest_challenge' } as QueueOptions;
    }

    public jobFunction = (data: SuggestChallengeOperationTaskData, operationProgress, operationResolve, operationReject) => {
        console.log('Suggesting challenge!');

        this.challengeSuggestionService.makeSuggestion(data.userId).then(() => {
            console.log('Suggestion made!');
            operationResolve();
        }).catch(error => {
            operationReject();
        });
    };
}

export class SuggestChallengeOperationTaskData extends TaskData {
    userId: string;
}