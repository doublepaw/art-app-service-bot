import { Operation } from '../infrastructure/Operation';
import { QueueOptions } from '../infrastructure/QueueOptions';
import { TaskData } from '../infrastructure/TaskData';
import { ChallengeAdminService } from '../services/ChallengeAdminService';

export class CompleteChallengeOperation extends Operation<CompleteChallengeOperationTaskData> {
    private challengeService: ChallengeAdminService;

    constructor() {
        super();
        this.challengeService = new ChallengeAdminService();
    }

    public getOptions() {
        return { specId: 'complete_challenge' } as QueueOptions;
    }

    public jobFunction = (data: CompleteChallengeOperationTaskData, operationProgress, operationResolve, operationReject) => {
        console.log('Completing challenge!');

        this.challengeService.completeChallenge(data.userId, data.activeChallengeId).then(challenge => {
            console.log('All went well! Challenge complete!');
            operationResolve();
        }).catch((error) => {
            console.log('Something went awry! Challenge still in progress!');
            operationReject(error);
        });
    };
}

export class CompleteChallengeOperationTaskData extends TaskData {
    userId: string;
    activeChallengeId: string;
}