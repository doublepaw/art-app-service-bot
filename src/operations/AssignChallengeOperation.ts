import { Operation } from '../infrastructure/Operation';
import { QueueOptions } from '../infrastructure/QueueOptions';
import { TaskData } from '../infrastructure/TaskData';
import { ChallengeAdminService } from '../services/ChallengeAdminService';
import { ChallengeTemplate } from 'art-app-common';

export class AssignChallengeOperation extends Operation<AssignChallengeTaskData>  {
    private challengeTemplatesRef: any;
    private activeChallengesRef: any;
    private challengeService: ChallengeAdminService;

    constructor() {
        super();
        this.challengeService = new ChallengeAdminService();
    }

    public getOptions(): QueueOptions {
        return { specId: 'assign_challenge', numWorkers: 1 } as QueueOptions;
    };

    public jobFunction = (data: AssignChallengeTaskData, operationProgress, resolveOperation, rejectOperation) => {
        console.log('Assigning new challenge!');

        return this.challengeService.assignChallenge(data.userId, data.challengeTemplateId).then(() => {
            console.log('Challenge assigned!');
            resolveOperation();
        }).catch((error) => {
            console.log('Something wen awry! No challenge assigned!');
            rejectOperation(error);
        });
    };
}

export class AssignChallengeTaskData extends TaskData {
    public challengeTemplateId: string;
    public userId: string;
}