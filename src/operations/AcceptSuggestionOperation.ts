import { ChallengeSuggestion } from '../../../common/models/ChallengeSuggestion';
import { ChallengeSuggestionService } from '../services/ChallengeSuggestionService';
import { Operation } from '../infrastructure/Operation';
import { QueueOptions } from '../infrastructure/QueueOptions';
import { TaskData } from '../infrastructure/TaskData';
import { TaskService } from '../services/TaskService';
import { ChallengeAdminService } from '../services/ChallengeAdminService';
import * as _ from 'lodash';
import { KeyValue } from 'art-app-common';

export class AcceptSuggestionOperation extends Operation<AcceptSuggestionOperationTaskData> {
    private challengeService: ChallengeAdminService;
    private suggestionService: ChallengeSuggestionService;
    private taskService: TaskService;

    constructor() {
        super();
        this.challengeService = new ChallengeAdminService();
        this.suggestionService = new ChallengeSuggestionService();
        this.taskService = new TaskService();
    }

    public getOptions() {
        return { specId: 'accept_suggestion' } as QueueOptions;
    }

    public jobFunction = (data: AcceptSuggestionOperationTaskData, operationProgress, operationResolve, operationReject) => {
        console.log('Checking whether suggestion ' + data.challengeSuggestionId + ' exists');
        this.suggestionService.suggestionExists(data.userId, data.challengeSuggestionId).then(() => {
            console.log('Seems like it does!');
            console.log('Getting it!');
            return this.suggestionService.getSuggestion(data.userId, data.challengeSuggestionId).then(gottenSuggestion => {
                console.log('Got it!');
                console.log('Assigning it!');
                return this.assignChallenge(data.userId, gottenSuggestion.value.challengeTemplateId).then(() => {
                    console.log('Removing it now!');
                    return this.suggestionService.removeSuggestion(data.userId, data.challengeSuggestionId).then(removedSuggestion => {
                        console.log('Removed!');

                    });
                });
            });
        }).then(() => {
            console.log('Sent message to my friend to add it as an accepted challenge!');
            operationResolve();
        }).catch(error => {
            operationReject(error);
        });
    };

    private assignChallenge = (userId: string, challengeTemplateId: string) => {
        var task = {
            _state: 'assign_challenge_start',
            userId: userId,
            challengeTemplateId: challengeTemplateId
        } as TaskData;

        return this.taskService.pushTask(task);
    };
}

class SuggestionData {
    public userId: string;
    public challengeSuggestionId;
}

export class AcceptSuggestionOperationTaskData extends TaskData {
    userId: string;
    challengeSuggestionId: string;
}