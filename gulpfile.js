var gulp = require('gulp');
var typescript = require('gulp-typescript');
var requireDir = require('require-dir');
var uglify = require('gulp-uglify');

var typescriptProject = typescript.createProject('tsconfig.json');

requireDir('./tasks');

gulp.task('build', function() {
    if (process.env.NODE_ENV == "development") {
        console.log('Building for development');

        typescriptProject.src()
            .pipe(typescript())
            .pipe(gulp.dest('dist'));
    } else {
        console.log('Building for production');

        typescriptProject.src()
            .pipe(typescript())
            .pipe(uglify({ mangle: true }))
            .pipe(gulp.dest('dist'));
    }
});