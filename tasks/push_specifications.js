var gulp = require('gulp');
var glob = require('glob');
var fs = require('fs');
var firebase = require('firebase-admin');

gulp.task('spec_push', function() {
    firebase.initializeApp({
        credential: firebase.credential.cert('config/art-app-22a6c-firebase-adminsdk-xxzfj-02e496989f.json'),
        databaseURL: "https://art-app-22a6c.firebaseio.com",
        databaseAuthVariableOverride: {
            "canAddSpecs": true,
            "canProcessTasks": true,
            "uid": 'service-bot'
        }
    });


    var specData = {};

    var buildSpecPromise = new Promise((resolveAll, reject) => {
        glob('jobs/*.json', function(error, fileList) {

            var fileReadPromises = [];

            for (var filePath of fileList) {
                var fileReadPromise = new Promise((resolveFileRead, reject) => {
                    fs.readFile(filePath, function(err, data) {
                        Object.assign(specData, JSON.parse(data));
                        resolveFileRead();
                    });
                });

                fileReadPromises.push(fileReadPromise);
            }

            Promise.all(fileReadPromises).then(value => {
                console.log('All are complete!');
                resolveAll();
            })
        });
    });



    buildSpecPromise.then(() => {
        console.log('Seems all went well!');
        console.log('specData: ', specData);
        console.log('Whoop!');

        var databaseRef = firebase.app().database().ref('queue/specs');
        console.log('databaseRef: ', databaseRef.toString());
        databaseRef.set(specData).then(() => {
            console.log('Set completed! Value');
        }).catch(error => {
            console.log('Error: ', error);
        });
    });
});