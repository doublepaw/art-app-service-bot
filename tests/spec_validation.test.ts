import { last } from 'rxjs/operator/last';
import { expect } from 'chai';
import * as glob from 'glob';
import * as fs from 'fs';
import * as path from 'path';


describe('Spec files', () => {
  it('should exist', (existDone) => {
    glob('jobs/*.json', (error, fileList: any[]) => {

      expect(fileList).to.be.an('Array');
      expect(fileList).to.not.be.empty;

      existDone();

      fileList.forEach(filePath => {
        describe('spec file ' + path.basename(filePath), () => {
          it('should be correctly formatted JSON', (filePathDone) => {
            var fileData = fs.readFileSync(filePath, 'utf8');
            var parsedSpecFile = JSON.parse(fileData);

            describe('parsed job spec ' + path.basename(filePath), () => {

              it('should have at least one spec', () => {
                expect(parsedSpecFile).to.not.be.null;
                expect(parsedSpecFile).to.not.be.undefined;
                expect(parsedSpecFile).to.have.haveOwnProperty;
              });

              // (parsedSpec as Object).

              var properties = [
                { name: 'start_state', type: 'string' },
                { name: 'in_progress_state', type: 'string' },
                { name: 'finished_state', type: 'string' },
                { name: 'error_state', type: 'string' },
                { name: 'timeout', type: 'number' },
                { name: 'retries', type: 'number' }
              ];

              var numberOfSpecs = 0;
              for (var spec in parsedSpecFile) {
                if ((parsedSpecFile as Object).hasOwnProperty(spec))
                  numberOfSpecs++;
              }

              var previousSpecFinishedState = null;

              for (var spec in parsedSpecFile) {
                if ((parsedSpecFile as Object).hasOwnProperty(spec)) {

                  describe('spec ' + spec, () => {
                    properties.forEach((property, index) => {
                      it('should have own property ' + property.name, () => {
                        expect(parsedSpecFile[spec][property.name]).to.not.be.undefined;
                      });

                      it('should be a ' + property.type + ' or null', () => {
                        expect(parsedSpecFile[spec][property.name]).to.satisfy((propertyValue) => {
                          return propertyValue === null || typeof propertyValue === property.type;
                        });
                      });
                    });

                    if (previousSpecFinishedState !== null) {
                      // Important line here. The it-callback is actually executed after previousSpecFinishedState is set.
                      // So we save the value of it, and we are guaranteed that finishedState's value will not change.
                      var finishedState = previousSpecFinishedState;
                      it('should have the same start_state (' + parsedSpecFile[spec].start_state + ') as the previous spec finished_state (' + finishedState + ')', () => {
                        expect(parsedSpecFile[spec].start_state).to.equal(finishedState);
                      });
                    }

                    previousSpecFinishedState = parsedSpecFile[spec].finished_state;
                  });
                }
              }

              filePathDone();
            });
          });
        });
      });
    });
  });
});